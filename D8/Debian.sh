#!/bin/bash
adduser derupal sudo
su derupal
cd /var/www/
sudo chown root:root /var/www
sudo chmod 755 /var/www/
sudo chown -R www-data:www-data /var/www/*
sudo chmod -R 774 /var/www/*

# install stuff
apt-get update && apt-get upgrade -y
apt-get install -y git apache2 php7.3 php7.3-mbstring php7.3-zip php7.3-sqlite3 php7.3-dba php7.3-dev fail2ban ufw gnupg

# basic security
ufw allow 80 && ufw allow 22 && ufw allow 443 && ufw enable
service fail2ban start

# ssh security goes here

# mysql-server isn't readily available on D10??
sudo wget http://repo.mysql.com/mysql-apt-config_0.8.13-1_all.deb
sudo dpkg -i mysql-apt-config_0.8.13-1_all.deb
sudo apt update && sudo apt install -y mysql-server
rm mysql-apt-config_0.8.13-1_all.deb
sudo systemctl restart mysql.service
sudo mysql_secure_installation

# apache config
sudo a2enmod rewrite
sudo systemctl restart apache2
cd /etc/apache2
echo '<Directory /var/www/*/web/>
  RewriteEngine on
  RewriteBase /
  RewriteCond %{REQUEST_FILENAME} !-f
  RewriteCond %{REQUEST_FILENAME} !-d
  RewriteRule ^(.*)$ index.php?q=$1 [L,QSA]
</Directory>' >> apache2.conf
# add swap just in case
sudo /bin/dd if=/dev/zero of=/var/swap.1 bs=1M count=1024
sudo /sbin/mkswap /var/swap.1
sudo /sbin/swapon /var/swap.1

# recommended method of D8 install
wget https://raw.githubusercontent.com/composer/getcomposer.org/76a7060ccb93902cd7576b67264ad91c8a2700e2/web/installer -O - -q | php -- --quiet
sudo mv composer.phar /usr/local/bin/composer
composer create-project drupal-composer/drupal-project:8.x-dev my_site_name_dir --no-interaction
cd my_site_name_dir
composer install --no-dev

# create DB
mysql -u root -p -e "CREATE DATABASE derupalDB CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci";
mysql -u root -p

# mysql stuff
mysql CREATE USER 'derupal'@'localhost' IDENTIFIED BY 'Pass30RDNEE0570BEch@ngED!';
GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, INDEX, ALTER, CREATE TEMPORARY TABLES ON derupalDB.* TO derupal@localhost;
FLUSH PRIVILEGES;
exit

# lock-up drupal
chmod u+w sites/default/settings.php

# blast-off
service apcahe2 start
mv drupal* /var/www/drupal
